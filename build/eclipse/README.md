# Eclipse generator

Execute the following command to generate an Xcode project for the source files in this repo:

```
$ cmake -G "Eclipse CDT4 - Unix Makefiles" ../..
```