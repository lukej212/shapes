#include "ShapeParameterException.h"

ShapeParameterException::ShapeParameterException(const std::string& message) 
    : std::invalid_argument("Shape parameter exception: " + message) {
}
