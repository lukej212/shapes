#include "Rectangle.h"

#ifndef __SQUARE_H__
#define __SQUARE_H__

class Square : public Rectangle {
public:
    // Initializing constructor
    Square(const length_unit side) throw(ShapeParameterException);

    // getter and setter
    length_unit getSide() const;
    void setSide(const length_unit side);

    // overrides
    virtual length_unit getLength() const override;
    virtual void setLength(const length_unit length) throw(ShapeParameterException) override;
    virtual length_unit getWidth() const override;
    virtual void setWidth(const length_unit width) throw(ShapeParameterException) override;
    virtual length_unit getPerimeter() const override;
    virtual area_unit getArea() const override;
    virtual std::string getName() const override;

    virtual ~Square() {}

private:
    void setDimensions(const length_unit value);
};

#endif // __SQUARE_H__
