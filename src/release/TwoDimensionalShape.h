#include "Shape.h"

#ifndef __TWO_DIMENSIONAL_SHAPE_H__
#define __TWO_DIMENSIONAL_SHAPE_H__

class TwoDimensionalShape : public Shape {
public:
    virtual length_unit getPerimeter() const = 0;
    virtual area_unit getArea() const = 0;
};

#endif // __TWO_DIMENSIONAL_SHAPE_H__
